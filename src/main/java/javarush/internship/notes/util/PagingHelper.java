package javarush.internship.notes.util;

import java.util.ArrayList;
import java.util.List;

public class PagingHelper {

    private int rowsOnPage;
    private CountableData countableData;

    private int currentPage;
    private int maxPage;

    private long currentDataCount;

    private List<PageLink> pageLinks;

    public PagingHelper(int rowsOnPage, CountableData countableData) {
        this.rowsOnPage = rowsOnPage;
        this.countableData = countableData;

        pageLinks = new ArrayList<>();
    }

    private void calculateMaxPage() {
        currentDataCount = countableData.getDataCount();
        maxPage = (int)(currentDataCount / rowsOnPage + (currentDataCount % rowsOnPage == 0 ? 0 : 1));
    }

    private int validatePage(int page) {
        calculateMaxPage();
        if (page < 0)
            page = 0;
        else if (page >= maxPage)
            page = maxPage - 1;
        return page;
    }

    public void setLastPage()
    {
        currentPage = Integer.MAX_VALUE;
    }
    public void setPage(int page)
    {
        currentPage = page;
    }

    public DataLimitSelectInfo getDataSelectInfo() {
        return getDataSelectInfo(currentPage);
    }

    public DataLimitSelectInfo getDataSelectInfo(int page) {
        currentPage = validatePage(page);

        int from = currentPage * rowsOnPage;
        int count = rowsOnPage;

        pageLinks = generatePageLinks();

        return new DataLimitSelectInfo(from, count);
    }

    public List<PageLink> getPageLinks() {
        return pageLinks;
    }

    public long getCurrentDataCount() {
        return currentDataCount;
    }

    private List<PageLink> generatePageLinks() {
        List<PageLink> links = new ArrayList<>();

        if (currentPage > 0)
            links.add(new PageLink("Предыдущая страница", currentPage - 1));

        for (int pageNumber = 0; pageNumber < maxPage; pageNumber++)
            links.add(new PageLink(String.valueOf(pageNumber + 1), pageNumber, pageNumber == currentPage));

        if (currentPage < maxPage - 1)
            links.add(new PageLink("Следующая страница", currentPage + 1));

        return links;
    }

    public static class PageLink {
        private String linkText;
        private int pageNumber;
        private boolean isCurrent;

        public PageLink(String linkText, int pageNumber) {
            this(linkText, pageNumber, false);
        }

        public PageLink(String linkText, int pageNumber, boolean isCurrent) {
            this.linkText = linkText;
            this.pageNumber = pageNumber;
            this.isCurrent = isCurrent;
        }
    }

    public static class DataLimitSelectInfo {
        private int from;
        private int count;

        public DataLimitSelectInfo(int from, int count) {
            this.from = from;
            this.count = count;
        }

        public int getFrom() {
            return from;
        }

        public int getCount() {
            return count;
        }
    }
}