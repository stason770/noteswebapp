package javarush.internship.notes.util;

public interface CountableData {
    long getDataCount();
}
