package javarush.internship.notes.controller;

import javarush.internship.notes.dao.NoteDao;
import javarush.internship.notes.entity.Note;
import javarush.internship.notes.util.PagingHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

@Controller
public class NoteController {

    @Autowired
    private NoteDao noteRepo;
    private PagingHelper pagingHelper = null;

    private String filterType;
    private String sortType;

    public PagingHelper getPagingHelper() {
        if (pagingHelper == null)
            pagingHelper = new PagingHelper(10, noteRepo);
        return pagingHelper;
    }

    public NoteController() {
        sortType = "Сначала новые";
        filterType = "Все";
    }
    private String getNoteList(Map<String, Object> model) {
        model.put("notes_data", noteRepo.getList(getPagingHelper().getDataSelectInfo()));
        model.put("notes_links", getPagingHelper().getPageLinks());
        model.put("notes_count", getPagingHelper().getCurrentDataCount());

        model.put("filterType", filterType);
        model.put("sortType", sortType);
        return "noteList";
    }

    @GetMapping("filter")
    public String filter(@RequestParam(name = "filterType", required = false, defaultValue = "all") String filterType, Map<String, Object> model) {
        switch (filterType) {
            case "all":
                noteRepo.setFilter(null);
                this.filterType = "Все";
                break;
            case "done":
                noteRepo.setFilter(Note.NoteStringFilterAndSort.onlyDoneFilter());
                this.filterType = "Выполненные";
                break;
            case "undone":
                noteRepo.setFilter(Note.NoteStringFilterAndSort.onlyUndoneFilter());
                this.filterType = "Не выполненные";
                break;
        }
        return getNoteList(model);
    }
    @GetMapping("sort")
    public String sort(@RequestParam(name = "sortType", required = false, defaultValue = "newFirst") String sortType, Map<String, Object> model) {
        switch (sortType) {
            case "newFirst":
                noteRepo.setComparator(Note.NoteStringFilterAndSort.newFirstSort());
                this.sortType = "Сначала новые";
                break;
            case "oldFirst":
                noteRepo.setComparator(Note.NoteStringFilterAndSort.oldFirstSort());
                this.sortType = "Сначала старые";
                break;
        }
        //getPagingHelper().setPage(0);
        return getNoteList(model);
    }

    @GetMapping
    public String index(@RequestParam(name = "page", required = false, defaultValue = "-1") Integer page, Map<String, Object> model) {
        if (page != -1)
            getPagingHelper().setPage(page);
        return getNoteList(model);
    }

    @GetMapping("add")
    public String add(Map<String, Object> model) {
        return "addNote";
    }
    @GetMapping("edit")
    public String edit(@RequestParam(name = "id", required = true) Integer id, Map<String, Object> model) {
        Note note = noteRepo.get(id);

        if (note != null) {
            model.put("id", id);
            model.put("text", note.getText());
            model.put("isDone", note.isDone());
            model.put("creationTime", note.getCreationTime());
            return "editNote";
        } else return getNoteList(model);
    }
    @GetMapping("remove")
    public String remove(@RequestParam(name = "id", required = true) Integer id, Map<String, Object> model) {
        Note note = noteRepo.get(id);
        if (note != null) {
            model.put("id", id);
            model.put("text", note.getText());
            model.put("isDone", note.isDone());
            return "removeConfirm";
        } else return getNoteList(model);
    }

    @PostMapping("add_post")
    public String add_post(@RequestParam String text, @RequestParam(required = false, defaultValue = "false") boolean isDone, Map<String, Object> model) {
        Note note = new Note();
        note.setText(text);
        note.setDone(isDone);
        note.setCreationTime(new Timestamp(new Date().getTime()));
        noteRepo.add(note);
        return "redirect:";
    }
    @PostMapping("edit_post")
    public String edit_post(@RequestParam int id, @RequestParam String text, @RequestParam(required = false, defaultValue = "false") boolean isDone, Map<String, Object> model) {
        Note note = new Note();
        note.setId(id);
        note.setText(text);
        note.setDone(isDone);
        noteRepo.update(note);
        return "redirect:";
    }
    @PostMapping("remove_post")
    public String remove_post(@RequestParam int id, Map<String, Object> model) {
        noteRepo.delete(id);
        return "redirect:";
    }
}
