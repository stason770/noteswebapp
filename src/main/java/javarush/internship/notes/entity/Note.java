package javarush.internship.notes.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "notes")
public class Note {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name="text", length = 100, nullable = false)
    private String text;

    @Column(name="isDone", nullable = false)
    private boolean isDone;

    @Column(name="creationTime", nullable = false)
    private Date creationTime;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }


    public static class NoteStringFilterAndSort {
        public static String onlyDoneFilter() {
            return "where isDone = true";
        }
        public static String onlyUndoneFilter() {
            return "where isDone = false";
        }
        public static String newFirstSort() {
            return "ORDER BY creationTime DESC";
        }
        public static String oldFirstSort() {
            return "ORDER BY creationTime";
        }
    }
}
