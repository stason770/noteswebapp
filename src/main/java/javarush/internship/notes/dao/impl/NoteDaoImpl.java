package javarush.internship.notes.dao.impl;

import javarush.internship.notes.dao.NoteDao;
import javarush.internship.notes.entity.Note;
import javarush.internship.notes.util.PagingHelper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import javax.persistence.Query;
import java.util.Collections;
import java.util.List;

@Repository
public class NoteDaoImpl implements NoteDao {
    private String filter;
    private String comparator;

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public List<Note> getList() {
        Session session = null;
        try {
            session = this.sessionFactory.openSession();
            StringBuilder queryString = new StringBuilder("from Note");

            if (filter != null)
                queryString.append(filter + " ");
            if (comparator != null)
                queryString.append(comparator);

            Query query = session.createQuery(queryString.toString());
            List<Note> notes = query.getResultList();
            return notes;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Collections.emptyList();
        }
    }

    @Override
    public List<Note> getList(PagingHelper.DataLimitSelectInfo selectInfo) {
        Session session = null;
        try {
            session = this.sessionFactory.openSession();
            StringBuilder queryString = new StringBuilder("from Note ");
            if (filter != null)
                queryString.append(filter + " ");
            if (comparator != null)
                queryString.append(comparator + " ");

            Query query = session.createQuery(queryString.toString());
            query.setFirstResult(selectInfo.getFrom());
            query.setMaxResults(selectInfo.getCount());

            List<Note> notes = query.getResultList();
            return notes;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Collections.emptyList();
        } finally {
            if (session != null)
                session.close();
        }
    }

    @Override
    public long getDataCount() {
        Session session = null;
        try {
            session = this.sessionFactory.openSession();
            StringBuilder queryString = new StringBuilder("SELECT count(*) from Note ");
            if (filter != null)
                queryString.append(filter + " ");

            Query query = session.createQuery(queryString.toString());
            long dataCount = (long) query.getSingleResult();
            return dataCount;
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        } finally {
            if (session != null)
                session.close();
        }
    }


    public NoteDaoImpl() {
        this.setComparator(Note.NoteStringFilterAndSort.newFirstSort());
    }

    @Override
    public void setFilter(String filter) {
        this.filter = filter;
    }

    @Override
    public void setComparator(String comparator) {
        this.comparator = comparator;
    }


    @Override
    public Note get(int id) {
        Session session = null;
        try {
            session = this.sessionFactory.openSession();
            return session.get(Note.class, id);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            if (session != null)
                session.close();
        }
    }

    @Override
    public void add(Note obj) {

        Session session = null;
        try {
            session = this.sessionFactory.openSession();
            session.save(obj);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (session != null)
                session.close();
        }

    }

    @Override
    public void update(Note obj) {
        Session session = null;
        try {
            session = this.sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            Note note = session.get(Note.class, obj.getId());
            if (note != null) {
                note.setText(obj.getText());
                note.setDone(obj.isDone());
            }
            session.update(note);
            transaction.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (session != null)
                session.close();
        }
    }

    @Override
    public void delete(int id) {
        Session session = null;
        try {
            session = this.sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            Note note = session.get(Note.class, id);
            if (note != null) {
                session.remove(note);
            }
            transaction.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (session != null)
                session.close();
        }
    }
}
