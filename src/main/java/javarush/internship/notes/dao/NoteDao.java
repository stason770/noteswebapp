package javarush.internship.notes.dao;

import javarush.internship.notes.entity.Note;
import javarush.internship.notes.util.CountableData;
import javarush.internship.notes.util.PagingHelper;

import java.util.List;

public interface NoteDao extends CountableData {
    List<Note> getList();
    List<Note> getList(PagingHelper.DataLimitSelectInfo selectInfo);

    void setFilter(String filter);
    void setComparator(String comparator);

    Note get(int id);
    void add(Note obj);
    void update(Note obj);
    void delete(int id);
}
